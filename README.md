# anaconda2019

This is the associated repo for the anacondacon2019 talk AI Driven Cybersecurity in 4 Easy Pieces!

This repo contains all the code necessary to replicate what was shown during the demo portion of the talk.

This demo was designed to be run on a single machine for purely testing and experiment purposes.

## About

This repo is meant to demonstrate a workflow for a data scientist to prepare data, develop models, deploy models, and monitor their predictions with easy to use open source tools.

The application of this workflow is to deploy an anomoly detection model for network traffic to a vulnerable web application. 

For a more detailed overview and diagram go to `notebooks/step_0_overview.ipynb`.

## Requirements

The necessary requirements to run this repo are

- [Minikube](https://github.com/kubernetes/minikube) version 0.30.0
- [Docker](https://docs.docker.com/install/) version 18.09.1
- [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/)
- [helm](https://helm.sh/docs/using_helm/)

### 1. Start a minikube cluster

This repo runs ontop of docker and kubernetes. To start your own cluster on your local machine, use `minikube` 
to create a local cluster. In addition mount the current repo directory so changes to notebooks will persist on disk

``` shell
minikube profile anacondacon2019
minikube start --profile=anacondacon2019
minikube mount $(pwd):/mnt &
```

### 2. Build containers

After running a minikube cluster, build the associated containers under your minikube local registry.

``` shell
eval $(minikube docker-env)
docker build -t sensor docker/sensor
docker build -t jupyter docker/jupyter
docker build -t user --build-arg file=user/user.py docker/users
docker build -t attacker --build-arg file=attacker/attack.py docker/users
```

This step may take a while ... 

### 3. Deploy cluster components

Next we will install applications for kubernetes in order to facilitate deployment of models, data management, and our data sources. These applications are valuable cluster components which we will deploy using `helm` and `kubectl`.

``` shell		
## Install seldon for model deployment
helm install seldon-core-crd \
	--name seldon-core-crd \
	--repo https://storage.googleapis.com/seldon-charts \
    --set usage_metrics.enabled=true
helm install seldon-core \
	 --name seldon-core \
	 --repo https://storage.googleapis.com/seldon-charts \
     --set ambassador.enabled=true \
     --set single_namespace=false

## Install minio for data storage
helm install stable/minio \
    --name anacondacon2019 \
	--set accessKey=anacondacon2019,secretKey=anacondacon2019 
    	
## Deploy pods 
kubectl apply -f ./yml/jupyter.yml
kubectl apply -f ./yml/attack_scenario.yml
```

### 4. Interacting

To access the demo notebooks first forward port from the jupyter pod and navigate to the notebooks directory

``` shell
sh -c "(kubectl port-forward svc/dvwa 8080:80 &\
        kubectl port-forward svc/anacondacon2019-minio 9000:9000 &\
        kubectl port-forward svc/jupyter 8888:8888)"
```
