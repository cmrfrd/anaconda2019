FROM continuumio/miniconda3

## Install os dependencies
RUN apt-get update &&\
    apt-get install -y python3-pip jq &&\
    apt-get clean &&\
    pip3 install yq &&\
    conda update conda

## Install conda environment
ENV ENVIRONMENT=/environment.yml
ADD environment.yml $ENVIRONMENT
RUN conda create -n $(cat $ENVIRONMENT | yq -r .name ) pip &&\
    export PATH=/opt/conda/envs/$(cat $ENVIRONMENT | yq -r .name )/bin:$PATH &&\
    conda env update -f /environment.yml
RUN echo "source activate $(cat $ENVIRONMENT | yq -r .name )" > ~/.bashrc

## Add user file
ARG file
ADD dvwa_lib.py /dvwa_lib.py
ADD ${file} /run.py
CMD ["bash", "-ic", "python3 /run.py"]
