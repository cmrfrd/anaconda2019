'''
user.py - alexander comerford - alexanderjcomerford@gmail.com

This script is a while true python progam designed to send random pre defined
payloads to mimick user based traffic to send to the "Damn vulnerable web application" 
via library commands located in `dvwa_lib.py`
'''

import os
import re
import sys
import time
import string
import random
import requests
from lxml import etree
from bs4 import BeautifulSoup

from dvwa_lib import *

## Read paramaters from environment
RETRIES=os.getenv('RETRIES',5)
INITIAL_SLEEP=os.getenv('INITIAL_SLEEP',5)
REQUEST_DELAY=os.getenv('REQUEST_DEPLAY',30)
DOMAIN=os.getenv('DOMAIN','http://localhost:80')
time.sleep(INITIAL_SLEEP)

if __name__=="__main__":

    ## Get initial session and token
    for r in range(RETRIES):
        try:
            session_id, user_token = csrf_token(DOMAIN)
            if setup_dvwa(session_id, user_token, DOMAIN):
                break
        except:
            print("Attempt %d..."%r)
            continue

    ## payloads to sample from
    commands = [
        lambda: "google.com",
        lambda: "github.com",
        lambda: "facebook.com",
        lambda: "redit.com",
        lambda: "yahoo.com",
        lambda: "twitter.com",
        lambda: "amazon.com",
        lambda: "youtube.com",        
        lambda: ''.join(random.choice(vocabulary) for x in range(20))
    ]

    while True:

        try:

            ## Select payload content
            command = random.choice(commands)()
            print ("Sent Command: %s"%command)

            ## Send payload
            response = send_command_injection(command, user_token, session_id, DOMAIN)

            ## Parse and log response content
            s = BeautifulSoup(response.content)
            response_command_result = s.body.find("pre").text
            print (response_command_result)

        except AttributeError:
            pass
