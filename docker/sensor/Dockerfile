FROM continuumio/miniconda3
ENV DEBIAN_FRONTEND=noninteractive

## Install os dependencies
RUN apt-get update &&\
    apt-get install -y python3-pip tshark jq &&\
    apt-get clean &&\
    pip3 install yq &&\
    conda update conda

## Install conda environment
ENV ENVIRONMENT=/environment.yml
ADD environment.yml $ENVIRONMENT
RUN conda create -n $(cat $ENVIRONMENT | yq -r .name ) pip &&\
    export PATH=/opt/conda/envs/$(cat $ENVIRONMENT | yq -r .name )/bin:$PATH &&\
    conda env update -f /environment.yml
RUN echo "source activate $(cat $ENVIRONMENT | yq -r .name )" > ~/.bashrc

ADD sensor.py /sensor.py
CMD ["bash", "-ic", "python3 /sensor.py"]
