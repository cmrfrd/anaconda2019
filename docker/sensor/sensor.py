'''
sensor.py - alexander comerford - alexanderjcomerford@gmail.com

This script is meant to be a network sensor event producer.

In batches, this script will upload network data as csv objects
in an object storage solution.
'''


import os
import re
import json
from datetime import datetime

import pyshark as pys
import pandas as pd
import minio as mn
from minio.error import (ResponseError, BucketAlreadyOwnedByYou,
                         BucketAlreadyExists)

## Read paramaters from the environment
BATCH_OBSERVATIONS=os.getenv('BATCH_OBSERVATIONS',5)
INTERFACE=os.getenv('INTERFACE','lo')
MINIO_DOMAIN=os.getenv('MINIO_DOMAIN','anacondacon2019-minio')
MINIO_ACCESS_KEY=os.getenv('MINIO_ACCESS_KEY','anacondacon2019')
MINIO_SECRET_KEY=os.getenv('MINIO_SECRET_KEY','anacondacon2019')

## Create capture
capture = pys.LiveCapture(interface=INTERFACE)

## Create minio client
minioClient = mn.Minio('%s:9000'%MINIO_DOMAIN,
                       access_key=MINIO_ACCESS_KEY,
                       secret_key=MINIO_SECRET_KEY,
                       secure=False)

## Base dataframe for pushing csv's
base_df = pd.DataFrame(columns=["time","content"])
send_df = base_df.copy(deep=True)
temp_df_filename = "/tmp/send_df.csv"

def latest(prefix="data", postfix=".csv"):
    '''Get string with latest time with prefix and postfix'''
    return ('{prefix}-{date:%Y-%m-%d_%H:%M:%S}{postfix}'.format(
        date=datetime.now(),
        prefix=prefix,
        postfix=postfix))

def minio_put_csv_file(minioClient, remote_filename, local_filename, bucket_name="data"):
    '''Given a file and a client, but file into bucket'''
    try:
        minioClient.make_bucket(bucket_name)
    except BucketAlreadyOwnedByYou as err:
        pass
    except BucketAlreadyExists as err:
        pass
    except ResponseError as err:
        raise

    try:
        minioClient.fput_object(bucket_name, remote_filename, local_filename,
                                content_type='application/csv')
        return True
    except ResponseError as err:
        print(err)
    return False

if __name__ == "__main__":

    counter = 0
    for packet in capture.sniff_continuously():
            
        if hasattr(packet, "http"):
            if hasattr(packet.http, "request_method") and \
               hasattr(packet.http, "_all_fields"):
                if packet.http.request_method == "POST":

                    ## Create dataframe
                    send_df = pd.concat([
                        send_df,
                        pd.DataFrame({
                            "time":datetime.now(),
                            "content": str(json.dumps(packet.http._all_fields))
                        }, index=[0])
                    ])
                

                    ## Send to minio if batch reached
                    if send_df.shape[0] >= BATCH_OBSERVATIONS:
                    
                        remote_filename = latest()
                        send_df.to_csv(temp_df_filename)
                        
                        response = minio_put_csv_file(minioClient,
                                                      local_filename=temp_df_filename,
                                                      remote_filename=remote_filename)

                        print("Sending: ",remote_filename,response)
                        send_df = base_df.copy(deep=True)
                        counter = 0
                        continue

                    print("Storing: %d requests ..."%counter)
                        
                    counter += 1
